# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.delivery import Delivery  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDeliveriesController(BaseTestCase):
    """DeliveriesController integration test stubs"""

    def test_deliveries_get(self):
        """Test case for deliveries_get

        Returns a list of deliveries.
        """
        response = self.client.open(
            '/v1/deliveries',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
