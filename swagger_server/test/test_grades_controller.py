# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.grade import Grade  # noqa: E501
from swagger_server.test import BaseTestCase


class TestGradesController(BaseTestCase):
    """GradesController integration test stubs"""

    def test_grades_get(self):
        """Test case for grades_get

        Returns a list of grades.
        """
        response = self.client.open(
            '/v1/grades',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
