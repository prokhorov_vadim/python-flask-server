import connexion
import six

from swagger_server.models.grade import Grade  # noqa: E501
from swagger_server import util


def grades_get():  # noqa: E501
    """Returns a list of grades.

    Returns a list of grades. # noqa: E501


    :rtype: Grade
    """
    return 'do some magic!'
