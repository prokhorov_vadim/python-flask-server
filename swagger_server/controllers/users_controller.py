import connexion
import six

from swagger_server.models.user import User  # noqa: E501
from swagger_server import util


def users_get():  # noqa: E501
    """Returns a list of users.

    Returns a list of users. # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def users_user_id_get(userId):  # noqa: E501
    """Returns a user by ID.

     # noqa: E501

    :param userId: Returns a user by ID.
    :type userId: int

    :rtype: User
    """
    return 'do some magic!'
