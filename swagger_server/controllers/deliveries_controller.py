import connexion
import six

from swagger_server.models.delivery import Delivery  # noqa: E501
from swagger_server import util


def deliveries_get():  # noqa: E501
    """Returns a list of deliveries.

    Returns a list of deliveries. # noqa: E501


    :rtype: Delivery
    """
    return 'do some  big magic!'
